from functions import m_json
from functions import m_pck


#path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
#metadata = m_json.get_metadata_from_setup(path)
#folder_path = "/home/pi/calorimetry_home/datasheets"
#metadata = m_json.add_temperature_sensor_serials(folder_path, metadata)
#data = m_pck.get_meas_data_calorimetry(metadata)
#json_folder = "/home/pi/calorimetry_home/datasheets"
#data_folder = "/home/pi/calorimetry_home/data/heat_capacity"
#m_pck.logging_calorimetry(data, metadata, data_folder, json_folder)
#folder_path = "/home/pi/calorimetry_home/datasheets"
#setup_path = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
#archiv_path = "/home/pi/calorimetry_home/data/heat_capacity"
#m_json.archiv_json(folder_path, setup_path, archiv_path)
path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)
folder_path = "/home/pi/calorimetry_home/datasheets"
metadata = m_json.add_temperature_sensor_serials(folder_path, metadata)
data = m_pck.get_meas_data_calorimetry(metadata)
json_folder = "/home/pi/calorimetry_home/datasheets"
data_folder = "/home/pi/calorimetry_home/data/newton"
m_pck.logging_calorimetry(data, metadata, data_folder, json_folder)
folder_path = "/home/pi/calorimetry_home/datasheets"
setup_path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
archiv_path = "/home/pi/calorimetry_home/data/newton"
m_json.archiv_json(folder_path, setup_path, archiv_path)